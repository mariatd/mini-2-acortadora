# Generated by Django 5.0.3 on 2024-04-18 21:16

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("acortad", "0001_initial"),
    ]

    operations = [
        migrations.CreateModel(
            name="Contenido",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("url", models.CharField(max_length=64)),
                ("short", models.TextField()),
            ],
        ),
        migrations.DeleteModel(
            name="ShortUrls",
        ),
    ]
