from django.urls import path

from . import views

urlpatterns = [
    path('', views.get_main),
    path('favicon.ico/', views.get_favicon),
    path('acortad/', views.index),
    path('acortad/<str:llave>/', views.get_content)
]
