from django.apps import AppConfig


class acortad(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'acortad'
