from django.shortcuts import render, redirect
from django.http import HttpResponse, Http404
from django.http import HttpResponseNotFound, HttpResponse
from django.urls import NoReverseMatch
from django.views.decorators.csrf import csrf_exempt
from django.template import loader
from django.core.validators import URLValidator
from django.core.exceptions import ValidationError
import re
import os
from .models import Contenido


def get_counter():
    counter = 1
    while Contenido.objects.filter(short=str(counter)).exists():
        counter += 1
    return counter


def get_main(request):
    return render(request, 'acortad/main.html', {'url_acortad': "acortad/"})


@csrf_exempt
def index(request):
    if request.method == "POST":
        url = request.POST.get("url", "")
        if url.strip():
            validator = URLValidator()
            try:
                validator(url)
            except ValidationError:
                if not re.match(r'^https?://|^www\.', url):
                    return HttpResponse("La URL debe comenzar con 'http://', 'https://' o 'www.'")
                else:
                    pass

            same_url = Contenido.objects.filter(url__endswith=url)
            if not same_url:
                if not request.POST.get("short", ""):
                    short = str(get_counter())
                else:
                    short = request.POST["short"]
                c = Contenido(url=url, short=short)
                c.save()

    content_list = Contenido.objects.all()
    template = loader.get_template('acortad/home.html')
    context = {
        'content_list': content_list
    }
    return HttpResponse(template.render(context, request))

@csrf_exempt
def get_content(request, llave):
    try:
        contenido = Contenido.objects.get(short=llave)
        return redirect(contenido.url)
    except Contenido.DoesNotExist:
        raise Http404("El recurso " + llave + "/ no existe")
    except NoReverseMatch:
        raise Http404("No se puede proporcionar una página porque la URL original que desea acortar: "
                      + contenido.url + " no es una URL válida")

def get_favicon(request):
    favicon_path = os.path.join(os.path.dirname(__file__), 'static', 'favicon.ico')

    if os.path.exists(favicon_path):
        with open(favicon_path, "rb") as f:
            favicon = f.read()
        return HttpResponse(favicon, content_type="image/x-icon")
    else:
        return HttpResponseNotFound()

